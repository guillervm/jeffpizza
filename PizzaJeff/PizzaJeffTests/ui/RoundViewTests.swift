//
//  RoundViewTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class RoundViewTests: XCTestCase {

    func testMakeViewRounded() {
        let view = RoundView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        XCTAssertEqual(view.cornerRadius, 50.0)
        view.bounds = CGRect(x: 0, y: 0, width: 200, height: 200)
        XCTAssertEqual(view.cornerRadius, 100.0)
        view.bounds = CGRect(x: 0, y: 0, width: 100, height: 200)
        XCTAssertEqual(view.cornerRadius, 50.0)
    }
}
