//
//  RoundCornersViewTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class RoundCornersViewTests: XCTestCase {

    func testRoundCorners() {
        let view = RoundCornersView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        XCTAssertEqual(view.cornerRadius, UIView.defaultCornerRadius)
        view.bounds = CGRect(x: 0, y: 0, width: 200, height: 200)
        XCTAssertEqual(view.cornerRadius, UIView.defaultCornerRadius)
        view.roundCorners(radius: 10.0)
        XCTAssertEqual(view.cornerRadius, 10.0)
    }
}
