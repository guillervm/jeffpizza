//
//  ShadowViewTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

@testable import PizzaJeff
import XCTest

final class ShadowViewTests: XCTestCase {

    func testApplyShadow() {
        let view = ShadowView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        XCTAssertEqual(view.layer.shadowColor, UIColor.black.cgColor)
        XCTAssertEqual(view.layer.shadowOpacity, 0.3)
        XCTAssertEqual(view.layer.shadowOffset, CGSize(width: 0, height: 2))
        XCTAssertEqual(view.layer.shadowRadius, 3)
        XCTAssertEqual(view.layer.shadowPath, UIBezierPath(rect: CGRect(x: 0, y: 0, width: 200, height: 100)).cgPath)
        XCTAssertEqual(view.layer.shouldRasterize, true)
        XCTAssertEqual(view.layer.rasterizationScale, UIScreen.main.scale)
        view.bounds = CGRect(x: 0, y: 0, width: 100, height: 200)
        XCTAssertEqual(view.layer.shadowColor, UIColor.black.cgColor)
        XCTAssertEqual(view.layer.shadowOpacity, 0.3)
        XCTAssertEqual(view.layer.shadowOffset, CGSize(width: 0, height: 2))
        XCTAssertEqual(view.layer.shadowRadius, 3)
        XCTAssertEqual(view.layer.shadowPath, UIBezierPath(rect: CGRect(x: 0, y: 0, width: 100, height: 200)).cgPath)
        XCTAssertEqual(view.layer.shouldRasterize, true)
        XCTAssertEqual(view.layer.rasterizationScale, UIScreen.main.scale)
    }
}
