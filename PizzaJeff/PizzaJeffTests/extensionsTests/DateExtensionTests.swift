//
//  DateExtensionTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class DateExtensionTests: XCTestCase {

    var date: Date!

    override func setUp() {
        super.setUp()
        NSTimeZone.default = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss.SSSS"
        formatter.timeZone = NSTimeZone.default
        date = formatter.date(from: "2021/06/21 15:00:00.0000")!
    }

    func testTimeMillisSince1970() {
        XCTAssertEqual(date.timeMillisSince1970(), 1624287600000)
    }
}
