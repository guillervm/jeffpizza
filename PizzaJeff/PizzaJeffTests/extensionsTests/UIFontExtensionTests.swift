//
//  UIFontExtensionTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class UIFontExtensionTests: XCTestCase {

    func testDefaultFont() {
        XCTAssertEqual(UIFont.defaultFont(), UIFont(name: "Lato-Regular", size: 16.0))
    }

    func testLatoLight() {
        XCTAssertEqual(UIFont.latoLight(), UIFont(name: "Lato-Light", size: 16.0))
        XCTAssertEqual(UIFont.latoLight(size: 20.0), UIFont(name: "Lato-Light", size: 20.0))
    }

    func testLatoRegular() {
        XCTAssertEqual(UIFont.latoRegular(), UIFont(name: "Lato-Regular", size: 16.0))
        XCTAssertEqual(UIFont.latoRegular(size: 20.0), UIFont(name: "Lato-Regular", size: 20.0))
    }

    func testLatoBold() {
        XCTAssertEqual(UIFont.latoBold(), UIFont(name: "Lato-Bold", size: 16.0))
        XCTAssertEqual(UIFont.latoBold(size: 20.0), UIFont(name: "Lato-Bold", size: 20.0))
    }

    func testCustomFont() {
        XCTAssertEqual(UIFont.customFont(name: "Helvetica", size: 16.0), UIFont(name: "Helvetica", size: 16.0))
    }
}
