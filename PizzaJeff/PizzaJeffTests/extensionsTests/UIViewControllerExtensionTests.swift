//
//  UIViewControllerExtensionTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class UIViewControllerExtensionTests: XCTestCase {

    func testLoadFromNib() {
        let vc = PickSegmentViewController.loadFromNib()
        XCTAssertNotNil(vc)
    }

    func testAddAsChild() {
        let vc = UIViewController()
        let childrenCount = vc.children.count
        vc.addAsChild(viewController: UIViewController(), in: vc.view, animated: false)
        XCTAssertEqual(vc.children.count, childrenCount + 1)
    }

    func testRemoveChild() {
        let vc = UIViewController()
        let vcToRemove = UIViewController()
        let childrenCountInitial = vc.children.count
        vc.addAsChild(viewController: UIViewController(), in: vc.view, animated: false)
        vc.addAsChild(viewController: vcToRemove, in: vc.view, animated: false)
        let childrenCount = vc.children.count
        vc.remove(viewController: vcToRemove, animated: false)
        XCTAssertEqual(vc.children.count, childrenCount - 1)
        XCTAssertEqual(vc.children.count, childrenCountInitial + 1)
        XCTAssertFalse(vc.children.contains(vcToRemove))

        let vc2 = UIViewController()
        let vc2ChildA = UIViewController()
        let vc2ChildB = UIViewController()
        let childrenCount2Initial = vc2.children.count
        vc2.addAsChild(viewController: vc2ChildA, in: vc2.view, animated: false)
        vc2.addAsChild(viewController: vc2ChildB, in: vc2.view, animated: false)
        let childrenCount2 = vc2.children.count
        vc2.remove(viewController: UIViewController(), animated: false)
        XCTAssertEqual(vc2.children.count, childrenCount2)
        XCTAssertEqual(vc2.children.count, childrenCount2Initial + 2)
        XCTAssertTrue(vc2.children.contains(vc2ChildA))
        XCTAssertTrue(vc2.children.contains(vc2ChildB))
    }

    func testRemoveNotification() {
        final class MockVCToRemove: UIViewController {
            var removed = false

            override func removeFromParent() {
                removed = true
            }
        }

        let uiViewController = UIViewController()
        let uiViewControllerToRemove = MockVCToRemove()
        uiViewController.addAsChild(viewController: uiViewControllerToRemove, in: uiViewController.view, animated: false)
        XCTAssertEqual(uiViewControllerToRemove.removed, false)
        uiViewController.remove(viewController: uiViewControllerToRemove, animated: false)
        XCTAssertEqual(uiViewControllerToRemove.removed, true)
    }

    func testRemoveAllChildViewControllers() {
        let uiViewController = UIViewController()
        let uiViewControllerChildA = UIViewController()
        let uiViewControllerChildB = UIViewController()
        let childrenCountInitial = uiViewController.children.count
        uiViewController.addAsChild(viewController: uiViewControllerChildA, in: uiViewController.view, animated: false)
        uiViewController.addAsChild(viewController: uiViewControllerChildB, in: uiViewController.view, animated: false)
        let childrenCount = uiViewController.children.count
        uiViewController.removeAllChildViewControllers()
        XCTAssertEqual(uiViewController.children.count, childrenCount - 2)
        XCTAssertEqual(uiViewController.children.count, childrenCountInitial)
        XCTAssertEqual(childrenCountInitial, childrenCount - 2)
        XCTAssertEqual(uiViewController.children.isEmpty, true)
        XCTAssertFalse(uiViewController.children.contains(uiViewControllerChildA))
        XCTAssertFalse(uiViewController.children.contains(uiViewControllerChildB))
    }
}
