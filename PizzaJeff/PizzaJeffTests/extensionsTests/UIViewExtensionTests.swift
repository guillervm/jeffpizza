//
//  UIViewExtensionTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

@testable import PizzaJeff
import XCTest

final class UIViewTests: XCTestCase {

    func createView(dimension: CGFloat = 100) -> UIView {
        return UIView(frame: CGRect(x: 0, y: 0, width: dimension, height: dimension))
    }

    /*
     Masked corners raw value calculation:
         kCALayerMinXMinYCorner = 1U << 0
         kCALayerMaxXMinYCorner = 1U << 1
         kCALayerMinXMaxYCorner = 1U << 2
         kCALayerMaxXMaxYCorner = 1U << 3
     */
    func testRoundCorners() {
        let viewAll = createView()
        viewAll.roundCorners()
        let viewTopLeftBottomRight = createView()
        viewTopLeftBottomRight.roundCorners(radius: 10, corners: [.topLeft, .bottomRight])
        XCTAssertEqual(viewAll.layer.cornerRadius, UIView.defaultCornerRadius)
        XCTAssertEqual(viewAll.layer.maskedCorners.rawValue, 15)
        XCTAssertEqual(viewTopLeftBottomRight.layer.cornerRadius, 10)
        XCTAssertEqual(viewTopLeftBottomRight.layer.maskedCorners.rawValue, 9)
    }

    func testFillContainerView() {
        let container = createView(dimension: 200)
        let child = createView()
        XCTAssertEqual(container.frame.width, 200)
        XCTAssertEqual(container.frame.height, 200)
        XCTAssertEqual(child.frame.width, 100)
        XCTAssertEqual(child.frame.height, 100)

        child.fillContainerView(container)
        XCTAssertEqual(container.subviews.count, 1)
        XCTAssertEqual(container.subviews.first, child)
        XCTAssertEqual(container.frame.width, 200)
        XCTAssertEqual(container.frame.height, 200)
        XCTAssertEqual(child.frame.width, 200)
        XCTAssertEqual(child.frame.height, 200)
    }
}
