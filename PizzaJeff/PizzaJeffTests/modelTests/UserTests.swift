//
//  UserTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class UserTests: XCTestCase {


    /*
     Considering 10 as a big enough sample for statistically having different icons when generating names randomly
     */
    func testRandomName() {
        var names: [String] = []
        for _ in 0..<10 {
            let name = User.randomName()
            if !names.contains(name) {
                names.append(name)
            }
        }
        XCTAssert(names.count > 1)
    }

    func testEquatable() {
        let userSegment0 = User(segment: .married, name: "Test", promocodes: ["TestCode2021"])
        let userSegment1 = User(segment: .single, name: "Test", promocodes: ["TestCode2021"])
        let userName0 = User(segment: .married, name: "Test0", promocodes: ["TestCode2021"])
        let userName1 = User(segment: .married, name: "Test1", promocodes: ["TestCode2021"])
        let userPromocodes0 = User(segment: .single, name: "Test", promocodes: ["TestCode2020"])
        let userPromocodes1 = User(segment: .single, name: "Test", promocodes: ["TestCode2021"])
        let userEqual0 = User(segment: .married, name: "Test", promocodes: ["TestCode2021"])
        let userEqual1 = User(segment: .married, name: "Test", promocodes: ["TestCode2021"])

        XCTAssertNotEqual(userSegment0, userSegment1)
        XCTAssertNotEqual(userName0, userName1)
        XCTAssertNotEqual(userPromocodes0, userPromocodes1)
        XCTAssertEqual(userEqual0, userEqual1)
    }
}
