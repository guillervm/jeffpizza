//
//  PizzaCartItemTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class PizzaCartItemTests: XCTestCase {

    func testDescription() {
        let pizzaM = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaXl = PizzaCartItem(id: 1, price: 1000, size: "XL", productInfo: DataFactory.productInfo(), amount: 10)
        XCTAssertEqual(pizzaM.description, "Size: M")
        XCTAssertEqual(pizzaXl.description, "Size: XL")
    }

    func testEquatable() {
        let pizzaItemId0 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemId1 = PizzaCartItem(id: 1, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemPrice0 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemPrice1 = PizzaCartItem(id: 0, price: 2000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemSize0 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemSize1 = PizzaCartItem(id: 0, price: 1000, size: "XL", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemAmount0 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 2)
        let pizzaItemAmount1 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemEqual0 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        let pizzaItemEqual1 = PizzaCartItem(id: 0, price: 1000, size: "M", productInfo: DataFactory.productInfo(), amount: 3)
        XCTAssertNotEqual(pizzaItemId0, pizzaItemId1)
        XCTAssertNotEqual(pizzaItemPrice0, pizzaItemPrice1)
        XCTAssertNotEqual(pizzaItemSize0, pizzaItemSize1)
        XCTAssertEqual(pizzaItemAmount0, pizzaItemAmount1)
        XCTAssertEqual(pizzaItemEqual0, pizzaItemEqual1)
    }
}
