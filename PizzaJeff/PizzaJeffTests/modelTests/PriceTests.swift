//
//  PriceTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

@testable import PizzaJeff
import XCTest

final class PriceTests: XCTestCase {

    func testInitFromResponse() {
        let jsonStr = "{\"size\": \"L\",\"price\": 13}"
        let jsonStrDecimal = "{\"size\": \"M\",\"price\": 11.5}"
        let menuItemResponse = try! JSONDecoder().decode(PriceResponse.self, from: jsonStr.data(using: .utf8)!)
        let menuItemDecimalResponse = try! JSONDecoder().decode(PriceResponse.self, from: jsonStrDecimal.data(using: .utf8)!)
        let price = Price(response: menuItemResponse)
        let priceDecimal = Price(response: menuItemDecimalResponse)
        XCTAssertEqual(price.size, "L")
        XCTAssertEqual(price.price, 1300)
        XCTAssertEqual(priceDecimal.size, "M")
        XCTAssertEqual(priceDecimal.price, 1150)
    }
}
