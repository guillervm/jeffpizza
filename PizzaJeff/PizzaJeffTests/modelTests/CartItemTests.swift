//
//  CartItemTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class CartItemTests: XCTestCase {

    func testDescription() {
        let item = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 0)
        XCTAssertEqual(item.description, "")
    }

    func testTotalPrice() {
        let item0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 0)
        let item1 = CartItem(id: 1, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let item2 = CartItem(id: 2, price: 1000, productInfo: DataFactory.productInfo(), amount: -10)
        XCTAssertEqual(item0.totalPrice(), 0)
        XCTAssertEqual(item1.totalPrice(), 10000)
        XCTAssertEqual(item2.totalPrice(), 0)
    }

    func testAdd() {
        let item0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 0)
        let item1 = CartItem(id: 1, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        item0.add(amount: 2)
        item1.add()
        XCTAssertEqual(item0.amount, 2)
        XCTAssertEqual(item1.amount, 11)
    }

    func testRemove() {
        let item0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let item1 = CartItem(id: 1, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let item2 = CartItem(id: 2, price: 1000, productInfo: DataFactory.productInfo(), amount: 0)
        let item3 = CartItem(id: 2, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        item0.remove(amount: 2)
        item1.remove()
        item2.remove()
        item3.remove(amount: 20)
        XCTAssertEqual(item0.amount, 8)
        XCTAssertEqual(item1.amount, 9)
        XCTAssertEqual(item2.amount, 0)
        XCTAssertEqual(item3.amount, 0)
    }

    func testEquatable() {
        let itemId0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let itemId1 = CartItem(id: 1, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let itemPrice0 = CartItem(id: 0, price: 500, productInfo: DataFactory.productInfo(), amount: 10)
        let itemPrice1 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let itemProductInfo0 = CartItem(id: 0, price: 1000, productInfo: ProductInfo(name: "Test0", content: "Test0", imageUrl: "Test0"), amount: 5)
        let itemProductInfo1 = CartItem(id: 0, price: 1000, productInfo: ProductInfo(name: "Test1", content: "Test1", imageUrl: "Test1"), amount: 10)
        let itemAmount0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 5)
        let itemAmount1 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 10)
        let itemEqual0 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 5)
        let itemEqual1 = CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 5)
        XCTAssertNotEqual(itemId0, itemId1)
        XCTAssertNotEqual(itemPrice0, itemPrice1)
        XCTAssertEqual(itemProductInfo0, itemProductInfo1)
        XCTAssertEqual(itemAmount0, itemAmount1)
        XCTAssertEqual(itemEqual0, itemEqual1)
    }
}
