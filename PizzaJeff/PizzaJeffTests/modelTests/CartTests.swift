//
//  CartTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class CartTests: XCTestCase {

    func testTotalAmount() {
        let cart = Cart(items: [
            CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2),
            CartItem(id: 1, price: 1500, productInfo: DataFactory.productInfo(), amount: 1),
            CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3)])
        let emptyCart = Cart()
        XCTAssertEqual(cart.totalAmount, 5000)
        XCTAssertEqual(emptyCart.totalAmount, 0)
    }

    func testAdd() {
        let cart = Cart(items: [
            CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2),
            CartItem(id: 1, price: 1500, productInfo: DataFactory.productInfo(), amount: 1),
            CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3)])
        cart.add(CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2))
        cart.add(CartItem(id: 3, price: 2000, productInfo: DataFactory.productInfo(), amount: 5))
        func getCartItem(id: UInt) -> CartItem? {
            return cart.items.first { item in
                item.id == id
            }
        }
        XCTAssertEqual(cart.items.count, 4)
        XCTAssertNotNil(getCartItem(id: 0))
        XCTAssertEqual(getCartItem(id: 0)!.amount, 4)
        XCTAssertNotNil(getCartItem(id: 1))
        XCTAssertEqual(getCartItem(id: 1)!.amount, 1)
        XCTAssertNotNil(getCartItem(id: 2))
        XCTAssertEqual(getCartItem(id: 2)!.amount, 3)
        XCTAssertNotNil(getCartItem(id: 3))
        XCTAssertEqual(getCartItem(id: 3)!.amount, 5)
    }

    func testRemove() {
        let cart = Cart(items: [
            CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 4),
            CartItem(id: 1, price: 1500, productInfo: DataFactory.productInfo(), amount: 1),
            CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3),
            CartItem(id: 3, price: 500, productInfo: DataFactory.productInfo(), amount: 3)])
        cart.remove(CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2))
        cart.remove(CartItem(id: 1, price: 1000, productInfo: DataFactory.productInfo(), amount: 1))
        cart.remove(CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3))
        cart.remove(CartItem(id: 3, price: 500, productInfo: DataFactory.productInfo(), amount: 5))
        cart.remove(CartItem(id: 4, price: 2000, productInfo: DataFactory.productInfo(), amount: 5))
        func getCartItem(id: UInt) -> CartItem? {
            return cart.items.first { item in
                item.id == id
            }
        }
        XCTAssertEqual(cart.items.count, 2)
        XCTAssertNotNil(getCartItem(id: 0))
        XCTAssertEqual(getCartItem(id: 0)!.amount, 2)
        XCTAssertNotNil(getCartItem(id: 1))
        XCTAssertEqual(getCartItem(id: 1)!.amount, 1)
        XCTAssertNil(getCartItem(id: 2))
        XCTAssertNil(getCartItem(id: 3))
        XCTAssertNil(getCartItem(id: 4))
    }

    func testClear() {
        let cart = Cart(items: [
            CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 4),
            CartItem(id: 1, price: 1500, productInfo: DataFactory.productInfo(), amount: 1),
            CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3),
            CartItem(id: 3, price: 500, productInfo: DataFactory.productInfo(), amount: 3)])
        XCTAssertEqual(cart.items.count, 4)
        cart.clear()
        XCTAssertEqual(cart.items.count, 0)
    }

    func testItemsCount() {
        let cart = Cart(items: [
            CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2),
            CartItem(id: 1, price: 1500, productInfo: DataFactory.productInfo(), amount: 1),
            CartItem(id: 2, price: 500, productInfo: DataFactory.productInfo(), amount: 3)])
        XCTAssertEqual(cart.itemsCount(), 6)
        cart.add(CartItem(id: 0, price: 1000, productInfo: DataFactory.productInfo(), amount: 2))
        XCTAssertEqual(cart.itemsCount(), 8)
    }
}
