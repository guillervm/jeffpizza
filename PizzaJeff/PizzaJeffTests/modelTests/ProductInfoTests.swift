//
//  ProductInfoTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class ProductInfoTests: XCTestCase {

    func testEquatable() {
        let productInfoName0 = ProductInfo(name: "Test0", content: "Test", imageUrl: "Test")
        let productInfoName1 = ProductInfo(name: "Test1", content: "Test", imageUrl: "Test")
        let productInfoContent0 = ProductInfo(name: "Test", content: "Test0", imageUrl: "Test")
        let productInfoContent1 = ProductInfo(name: "Test", content: "Test1", imageUrl: "Test")
        let productInfoImageUrl0 = ProductInfo(name: "Test", content: "Test", imageUrl: "Test0")
        let productInfoImageUrl1 = ProductInfo(name: "Test", content: "Test", imageUrl: "Test1")
        let productInfoEqual0 = ProductInfo(name: "Test", content: "Test", imageUrl: "Test")
        let productInfoEqual1 = ProductInfo(name: "Test", content: "Test", imageUrl: "Test")
        XCTAssertNotEqual(productInfoName0, productInfoName1)
        XCTAssertNotEqual(productInfoContent0, productInfoContent1)
        XCTAssertNotEqual(productInfoImageUrl0, productInfoImageUrl1)
        XCTAssertEqual(productInfoEqual0, productInfoEqual1)
    }
}
