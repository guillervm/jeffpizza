//
//  MenuItemTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

@testable import PizzaJeff
import XCTest

final class MenuItemTests: XCTestCase {

    func testInitFromResponse() {
        let jsonStr = "{\"id\": 1000,\"name\": \"Nueva! Pizza Fadrina\",\"content\": \"Base de tomate, mozzarella, emmental y 3 quesos originales de Castelló de la Plana: queso Espadán, queso Catí Pell Florida y queso Almassora Pimentó\",\"imageUrl\": \"https://d1ralsognjng37.cloudfront.net/a660278a-db4a-4d4d-a373-9110ca733ede.jpeg\",\"prices\": [{\"size\": \"S\",\"price\": 6},{\"size\": \"M\",\"price\": 8},{\"size\": \"L\",\"price\": 13},{\"size\": \"XL\",\"price\": 16}]}"
        let menuItemResponse = try! JSONDecoder().decode(MenuItemResponse.self, from: jsonStr.data(using: .utf8)!)
        let menuItem = MenuItem(response: menuItemResponse)
        XCTAssertEqual(menuItem.id, 1000)
        XCTAssertEqual(menuItem.name, "Nueva! Pizza Fadrina")
        XCTAssertEqual(menuItem.content, "Base de tomate, mozzarella, emmental y 3 quesos originales de Castelló de la Plana: queso Espadán, queso Catí Pell Florida y queso Almassora Pimentó")
        XCTAssertEqual(menuItem.imageUrl, "https://d1ralsognjng37.cloudfront.net/a660278a-db4a-4d4d-a373-9110ca733ede.jpeg")
        XCTAssertEqual(menuItem.prices.count, 4)
    }
}
