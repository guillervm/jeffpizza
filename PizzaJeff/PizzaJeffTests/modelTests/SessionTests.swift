//
//  SessionTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class SessionTests: XCTestCase {

    override func setUp() {
        Session.current?.endSession()
    }

    func testStartSession() {
        let user = DataFactory.user()
        XCTAssertNil(Session.current)
        Session.startSession(user: user)
        XCTAssertNotNil(Session.current)
        XCTAssertEqual(Session.current?.user, user)
        XCTAssertNotNil(Session.current?.cart)
    }

    func testEndSession() {
        XCTAssertNil(Session.current)
        let user = DataFactory.user()
        Session.startSession(user: user)
        XCTAssertNotNil(Session.current)
        Session.current?.endSession()
        XCTAssertNil(Session.current)
    }
}
