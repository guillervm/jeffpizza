//
//  MoneyFormatterTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

@testable import PizzaJeff
import XCTest

final class MoneyFormatterTests: XCTestCase {

    func testFormatAmount() {
        XCTAssertEqual(MoneyFormatter.formatAmount(5), "€0.05")
        XCTAssertEqual(MoneyFormatter.formatAmount(50), "€0.50")
        XCTAssertEqual(MoneyFormatter.formatAmount(500), "€5.00")
        XCTAssertEqual(MoneyFormatter.formatAmount(5000), "€50.00")
        XCTAssertEqual(MoneyFormatter.formatAmount(50000), "€500.00")
        XCTAssertEqual(MoneyFormatter.formatAmount(500000), "€5,000.00")
        XCTAssertEqual(MoneyFormatter.formatAmount(-500), "-€5.00")
    }
}
