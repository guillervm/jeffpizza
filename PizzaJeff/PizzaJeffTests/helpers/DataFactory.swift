//
//  DataFactory.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import Foundation

struct DataFactory {

    static func user() -> User {
        return User(segment: .married, name: "Test", promocodes: ["TesterCode2021"])
    }

    static func productInfo() -> ProductInfo {
        return ProductInfo(name: "Test", content: "Test", imageUrl: "Test")
    }
}
