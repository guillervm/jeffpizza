//
//  DebouncerTests.swift
//  PizzaJeffTests
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

@testable import PizzaJeff
import XCTest

final class DebouncerTests: XCTestCase {

    func testAction() {
        let debouncerDefault = Debouncer()
        let debouncerLong = Debouncer(debounceTimeMillis: 1000)
        XCTAssertTrue(debouncerDefault.action())
        XCTAssertTrue(debouncerLong.action())
        XCTAssertFalse(debouncerDefault.action())
        XCTAssertFalse(debouncerLong.action())
        Thread.sleep(forTimeInterval: 0.4)
        XCTAssertFalse(debouncerDefault.action())
        XCTAssertFalse(debouncerLong.action())
        Thread.sleep(forTimeInterval: 0.4)
        XCTAssertTrue(debouncerDefault.action())
        XCTAssertFalse(debouncerLong.action())
        Thread.sleep(forTimeInterval: 0.4)
        XCTAssertTrue(debouncerLong.action())
    }
}
