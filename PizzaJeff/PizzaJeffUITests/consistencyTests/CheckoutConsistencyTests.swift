//
//  CheckoutConsistencyTests.swift
//  PizzaJeffUITests
//
//  Created by Guillermo Velez de Mendizabal on 24/06/2021.
//

import XCTest

final class CheckoutConsistencyTests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func setUp() {
        let app = XCUIApplication()
        app.launchArguments = ["enable-uitesting"]
        app.launch()
    }

    override func tearDown() {
        XCUIApplication().terminate()
    }

    func testShoppingCartEmptyScreen() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["Checkout"].isEnabled)
        XCTAssertFalse(tablesQuery["TableCheckoutSummary"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Your shopping cart\nis empty"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Total:"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["€0.00"].waitForExistence(timeout: 1.0))
    }

    func testShoppingCartWithItemsScreen() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].tap()
        XCTAssertTrue(app.staticTexts["1"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].tap()
        XCTAssertTrue(app.staticTexts["2"].waitForExistence(timeout: 1.0))
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].isEnabled)
        XCTAssertFalse(app.staticTexts["Your shopping cart\nis empty"].exists)
        XCTAssertTrue(tablesQuery["TableCheckoutSummary"].waitForExistence(timeout: 1.0))
        XCTAssertEqual(tablesQuery["TableCheckoutSummary"].cells.count, 1)
        XCTAssertTrue(tablesQuery["TableCheckoutSummary"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 0).exists)
        XCTAssertTrue(tablesQuery["TableCheckoutSummary"].cells.containing(.staticText, identifier:"Size: L").children(matching: .other).element(boundBy: 0).exists)
        XCTAssertTrue(tablesQuery["TableCheckoutSummary"].cells.containing(.staticText, identifier:"2 × €12.00").children(matching: .other).element(boundBy: 0).exists)
        XCTAssertTrue(tablesQuery["TableCheckoutSummary"].cells.containing(.staticText, identifier:"€24.00").children(matching: .other).element(boundBy: 0).exists)
        XCTAssertTrue(app.staticTexts["Total:"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["€24.00"].waitForExistence(timeout: 1.0))
    }
}
