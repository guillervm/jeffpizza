//
//  SecondaryAppFlowsTests.swift
//  PizzaJeffUITests
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import XCTest

final class SecondaryAppFlowsTests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func setUp() {
        let app = XCUIApplication()
        app.launchArguments = ["enable-uitesting"]
        app.launch()
    }

    override func tearDown() {
        XCUIApplication().terminate()
    }

    func testProfileLogoutFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        app.buttons["IcProfile"].tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Profile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Logout"].waitForExistence(timeout: 1.0))
        app.buttons["Logout"].tap()

        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
    }

    func testProfileBackFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        app.buttons["IcProfile"].tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Logout"].waitForExistence(timeout: 1.0))
        app.buttons["IcBack"].tap()

        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
    }

    func testDetailsBackFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizza details"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        app.buttons["IcBack"].tap()

        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
    }

    func testShoppingCartBackFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        app.buttons["IcBack"].tap()

        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
    }

    func testDetailsShoppingCartBackFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizza details"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        app.buttons["IcBack"].tap()

        XCTAssertTrue(app.buttons["IcBack"].exists)
        XCTAssertFalse(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizza details"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        app.buttons["IcBack"].tap()

        XCTAssertTrue(tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).exists)
        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
    }
}
