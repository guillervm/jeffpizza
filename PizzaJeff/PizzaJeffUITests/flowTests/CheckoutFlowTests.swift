//
//  CheckoutFlowTests.swift
//  PizzaJeffUITests
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import XCTest

final class CheckoutFlowTests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func setUp() {
        let app = XCUIApplication()
        app.launchArguments = ["enable-uitesting"]
        app.launch()
    }

    override func tearDown() {
        XCUIApplication().terminate()
    }

    func testSingleCheckoutFlow() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Single"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertFalse(app.buttons["IcShoppingCart"].exists)
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        app.buttons["Checkout"].tap()

        XCTAssertTrue(app.staticTexts["Your order is on it's way!"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["OK"].waitForExistence(timeout: 1.0))
        app.buttons["OK"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].staticTexts["Pizza 0"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
    }

    func testMarriedCheckoutFlowWithMovie() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Married"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizza details"].waitForExistence(timeout: 1.0))
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertFalse(app.buttons["IcShoppingCart"].exists)
        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        app.buttons["Checkout"].tap()

        XCTAssertTrue(app.staticTexts["Fancy a movie?"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Order now"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Not today"].waitForExistence(timeout: 1.0))
        app.buttons["Order now"].tap()

        XCTAssertTrue(app.staticTexts["Your order is on it's way!"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["OK"].waitForExistence(timeout: 1.0))
        app.buttons["OK"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].staticTexts["Pizza 0"].waitForExistence(timeout: 1.0))
    }

    func testMarriedCheckoutFlowWithoutMovie() {

        let app = XCUIApplication()
        XCTAssertTrue(app.buttons["Single"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Married"].waitForExistence(timeout: 1.0))
        app.buttons["Married"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        let tablesQuery = app.tables
        tablesQuery["TablePizzasMenu"].cells.containing(.staticText, identifier:"Pizza 0").children(matching: .other).element(boundBy: 1).tap()

        XCTAssertTrue(tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].waitForExistence(timeout: 1.0))
        tablesQuery["TablePizzaPrices"].staticTexts["⭐️  L - €12.00  ⭐️"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizza details"].waitForExistence(timeout: 1.0))
        app.buttons["IcShoppingCart"].tap()

        XCTAssertTrue(app.buttons["IcBack"].waitForExistence(timeout: 1.0))
        XCTAssertFalse(app.buttons["IcProfile"].exists)
        XCTAssertFalse(app.buttons["IcShoppingCart"].exists)
        XCTAssertTrue(app.staticTexts["Checkout"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Checkout"].waitForExistence(timeout: 1.0))
        app.buttons["Checkout"].tap()

        XCTAssertTrue(app.staticTexts["Fancy a movie?"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Order now"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["Not today"].waitForExistence(timeout: 1.0))
        app.buttons["Not today"].tap()

        XCTAssertTrue(app.staticTexts["Your order is on it's way!"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["OK"].waitForExistence(timeout: 1.0))
        app.buttons["OK"].tap()

        XCTAssertFalse(app.buttons["IcBack"].exists)
        XCTAssertTrue(app.buttons["IcProfile"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.buttons["IcShoppingCart"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(app.staticTexts["Pizzas menu"].waitForExistence(timeout: 1.0))
        XCTAssertTrue(tablesQuery["TablePizzasMenu"].staticTexts["Pizza 0"].waitForExistence(timeout: 1.0))
    }
}
