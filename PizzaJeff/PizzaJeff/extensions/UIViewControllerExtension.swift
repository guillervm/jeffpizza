//
//  UIViewControllerExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

extension UIViewController {

    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }

    @objc func addAsChild(viewController: UIViewController, in containerView: UIView, animated: Bool, completion: (() -> ())? = nil) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        if animated {
            viewController.view.alpha = 0
            UIView.animate(withDuration: Animation.customNavigationDuration, animations: {
                viewController.view.alpha = 1
            }) { (finished) in
                completion?()
            }
        } else {
            viewController.view.frame = containerView.bounds
            completion?()
        }
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }

    @objc func remove(viewController: UIViewController, animated: Bool) {
        viewController.willMove(toParent: nil)
        if animated {
            UIView.animate(withDuration: Animation.customNavigationDuration, animations: {
                viewController.view.alpha = 0
            }) { (_) in
                viewController.view.removeFromSuperview()
            }
        } else {
            viewController.view.removeFromSuperview()
        }
        viewController.removeFromParent()
    }

    func removeAllChildViewControllers() {
        for child in children {
            remove(viewController: child, animated: false)
        }
    }
}
