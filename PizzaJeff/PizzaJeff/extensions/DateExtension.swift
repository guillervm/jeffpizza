//
//  DateExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

extension Date {

    func timeMillisSince1970() -> Int {
        return Int(timeIntervalSince1970 * 1000.0)
    }
}
