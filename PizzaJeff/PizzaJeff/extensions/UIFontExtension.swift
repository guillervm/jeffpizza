//
//  UIFontExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

extension UIFont {

    static func defaultFont() -> UIFont {
        return customFont(name: CustomFont.latoRegular, size: CustomFont.defaultFontSize)
    }

    static func latoLight(size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
        return customFont(name: CustomFont.latoLight, size: size)
    }

    static func latoRegular(size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
        return customFont(name: CustomFont.latoRegular, size: size)
    }

    static func latoBold(size: CGFloat = CustomFont.defaultFontSize) -> UIFont {
        return customFont(name: CustomFont.latoBold, size: size)
    }

    static func customFont(name: String, size: CGFloat) -> UIFont {
        let font = UIFont(name: name, size: size)
        assert(font != nil, "Can't load font: \(name)")
        return font ?? UIFont.systemFont(ofSize: size)
    }
}
