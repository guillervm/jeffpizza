//
//  UIViewExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

enum Corners: CaseIterable {

    case topLeft
    case topRight
    case bottomLeft
    case bottomRight

    func cornerMask() -> CACornerMask {
        switch self {
        case .topLeft:
            return [.layerMinXMinYCorner]
        case .topRight:
            return [.layerMaxXMinYCorner]
        case .bottomLeft:
            return [.layerMinXMaxYCorner]
        case .bottomRight:
            return [.layerMaxXMaxYCorner]
        }
    }
}

extension UIView {

    static let defaultCornerRadius: CGFloat = 4.0
    static let defaultBorderWidth: CGFloat = 1.0

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            guard let borderColor = self.layer.borderColor else { return nil}
            return UIColor(cgColor: borderColor)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }

    func roundCorners(radius: CGFloat = UIView.defaultCornerRadius, corners: [Corners] = [.topLeft, .topRight, .bottomLeft, .bottomRight]) {
        self.cornerRadius = radius
        var cornerOptionSet: CACornerMask = []
        for eachCorner in corners {
            cornerOptionSet.insert(eachCorner.cornerMask())
        }
        self.layer.maskedCorners = cornerOptionSet
    }

    func fillContainerView(_ container: UIView!) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.frame = container.frame
        container.addSubview(self)
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
