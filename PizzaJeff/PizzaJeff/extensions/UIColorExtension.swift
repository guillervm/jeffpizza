//
//  UIColorExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

extension UIColor {

    //TODO automate color extraction
    static let accent = UIColor(named: "AccentColor")!
    static let backgroundMain = UIColor(named: "BackgroundMainColor")!
    static let backgroundRed = UIColor(named: "BackgroundRedColor")!
    static let buttonSecondaryBackground = UIColor(named: "ButtonSecondaryBackgroundColor")!
    static let buttonSecondaryBackgroundDisabled = UIColor(named: "ButtonSecondaryBackgroundDisabledColor")!
    static let buttonSecondaryBackgroundHighlighted = UIColor(named: "ButtonSecondaryBackgroundHighlightedColor")!
    static let buttonSecondaryBorder = UIColor(named: "ButtonSecondaryBorderColor")!
    static let buttonStandardBackground = UIColor(named: "ButtonStandardBackgroundColor")!
    static let buttonStandardBackgroundDisabled = UIColor(named: "ButtonStandardBackgroundDisabledColor")!
    static let buttonStandardBackgroundHighlighted = UIColor(named: "ButtonStandardBackgroundHighlightedColor")!
    static let buttonTextDisabled = UIColor(named: "ButtonTextDisabledColor")!
    static let buttonTextHighlighted = UIColor(named: "ButtonTextHighlightedColor")!
    static let buttonTextMain = UIColor(named: "ButtonTextMainColor")!
    static let buttonTextSecondary = UIColor(named: "ButtonTextSecondaryColor")!
    static let buttonTextSecondaryDisabled = UIColor(named: "ButtonTextSecondaryDisabledColor")!
    static let buttonTextSecondaryHighlighted = UIColor(named: "ButtonTextSecondaryHighlightedColor")!
    static let foregroundMain = UIColor(named: "ForegroundMainColor")!
    static let loadingOverlay = UIColor(named: "LoadingOverlayColor")!
    static let main = UIColor(named: "MainColor")!
    static let pizzaJeff = UIColor(named: "PizzaJeffColor")!
    static let textMain = UIColor(named: "TextMainColor")!
    static let textMainInverse = UIColor(named: "TextMainInverseColor")!
    static let white = UIColor(named: "WhiteColor")!
}
