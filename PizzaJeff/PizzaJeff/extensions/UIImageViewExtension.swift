//
//  UIImageViewExtension.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

fileprivate let imageCache = NSCache<NSString, UIImage>()
fileprivate var imageDataTasks: [ObjectIdentifier : URLSessionDataTask] = [:]

extension UIImageView {

    func loadImage(urlString: String, useCache: Bool? = true, placeholder: UIImage? = nil) {
        doImageLoad(url: URL(string: urlString), useCache: useCache, placeholder: placeholder)
    }

    func loadImage(url: URL, useCache: Bool? = true, placeholder: UIImage? = nil) {
        doImageLoad(url: url, useCache: useCache, placeholder: placeholder)
    }

    private func doImageLoad(url: URL?, useCache: Bool? = true, placeholder: UIImage? = nil) {
        let id = ObjectIdentifier(self)
        if let dataTask = imageDataTasks[id] {
            dataTask.cancel()
            imageDataTasks.removeValue(forKey: id)
        }
        image = placeholder
        guard let url = url else {
            return
        }
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            image = cachedImage
            return
        }
        let dataTask = URLSession.shared.dataTask(with: url) { data, response, error in
            imageDataTasks.removeValue(forKey: id)
            DispatchQueue.main.async {
                if let data = data, let image = UIImage(data: data) {
                    self.image = image
                    imageCache.setObject(image, forKey: url.absoluteString as NSString)
                }
            }
        }
        imageDataTasks[id] = dataTask
        dataTask.resume()
    }
}
