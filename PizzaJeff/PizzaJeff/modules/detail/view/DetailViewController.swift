//
//  DetailViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

final class DetailViewController: UIViewController, RootToolbarProtocol, PresenterToViewDetailProtocol, PriceCellProtocol, UITableViewDelegate, UITableViewDataSource {

    var presenter: ViewToPresenterDetailProtocol?
    var displayProfile: Bool = false
    var displayShoppingCart: Bool = true
    var toolbarTitle: String = "Pizza details"
    var menuItem: MenuItem!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.delaysContentTouches = false
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: PriceCell.nibName, bundle: nil), forCellReuseIdentifier: PriceCell.identifier)
        tableView.register(UINib(nibName: DetailHeaderCell.nibName, bundle: nil), forCellReuseIdentifier: DetailHeaderCell.identifier)
        tableView.accessibilityIdentifier = "TablePizzaPrices"
    }

    //MARK: - PriceCellProtocol
    func priceSelected(price: Price) {
        presenter?.addToCart(item: menuItem, price: price)
    }

    //MARK: - UITableViewDelegate & UITableViewDataSource
    func cellForDetailHeader(_ tableView: UITableView, menuItem: MenuItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.identifier) as? DetailHeaderCell else {
            fatalError("Could not dequeue a cell for \(DetailHeaderCell.identifier)")
        }
        cell.bind(menuItem: menuItem)
        return cell
    }

    func cellForPrice(_ tableView: UITableView, price: Price) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PriceCell.identifier) as? PriceCell else {
            fatalError("Could not dequeue a cell for \(PriceCell.identifier)")
        }
        cell.delegate = self
        cell.bind(price: price)
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return cellForDetailHeader(tableView, menuItem: menuItem)
        }
        return cellForPrice(tableView, price: menuItem.prices[indexPath.row - 1])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Do nothing, button events will be managed
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return MenuItemCell.estimatedHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + menuItem.prices.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
