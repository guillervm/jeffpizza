//
//  PriceCell.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

protocol PriceCellProtocol: AnyObject {

    func priceSelected(price: Price)
}

final class PriceCell: UITableViewCell {

    static let identifier = "PriceCell"
    static let nibName = identifier
    static let estimatedHeight: CGFloat = 162

    @IBOutlet weak var btnPrice: StandardButton!

    weak var delegate: PriceCellProtocol?
    private var price: Price!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundView = UIView(frame: .zero)
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
    }

    @IBAction func priceTapped(_ sender: Any) {
        if let price = price, mainDebouncer.action() {
            delegate?.priceSelected(price: price)
        }
    }

    func bind(price newPrice: Price) {
        price = newPrice
        btnPrice.setTitle(String(format: "\(getMatchIndicator())  %1$@ - %2$@  \(getMatchIndicator())", price.size, MoneyFormatter.formatAmount(price.price)).trimmingCharacters(in: .whitespaces), for: .normal)
    }

    private func getMatchIndicator() -> String {
        guard let segment = Session.current?.user?.segment, segment.isPizzaSizeSatisfactory(price.size) else {
            return ""
        }
        return "⭐️"
    }
}
