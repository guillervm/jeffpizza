//
//  DetailHeaderView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

final class DetailHeaderCell: UITableViewCell {

    static let identifier = "DetailHeaderCell"
    static let nibName = identifier
    static let estimatedHeight: CGFloat = 504

    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContent: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundView = UIView(frame: .zero)
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
    }

    func bind(menuItem: MenuItem) {
        imgMain.loadImage(urlString: menuItem.imageUrl, placeholder: #imageLiteral(resourceName: "PizzaPlaceholder"))
        lblName.text = menuItem.name
        lblContent.text = menuItem.content
    }
}
