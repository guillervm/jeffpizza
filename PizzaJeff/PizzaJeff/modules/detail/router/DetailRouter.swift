//
//  DetailRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class DetailRouter: PresenterToRouterDetailProtocol {

    static func createDetailModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> DetailViewController {

        let view = DetailViewController.loadFromNib()
        let presenter = DetailPresenter()
        let interactor = DetailInteractor()
        let router = DetailRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        router.navigationDelegate = navigationDelegate
        router.shoppingCartDelegate = shoppingCartDelegate

        return view
    }

    var navigationDelegate: RootNavigationProtocol?
    var shoppingCartDelegate: ToolbarShoppingCartProtocol?
}
