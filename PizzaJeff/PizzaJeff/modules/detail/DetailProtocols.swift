//
//  DetailProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

protocol ViewToPresenterDetailProtocol: AnyObject {

    var view: PresenterToViewDetailProtocol? { get set }
    var interactor: PresenterToInteractorDetailProtocol? { get set }
    var router: PresenterToRouterDetailProtocol? { get set }

    func addToCart(item: MenuItem, price: Price)
}

protocol PresenterToViewDetailProtocol: AnyObject {

    var presenter: ViewToPresenterDetailProtocol? { get set }
}

protocol PresenterToRouterDetailProtocol: AnyObject {

    static func createDetailModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> DetailViewController

    var shoppingCartDelegate: ToolbarShoppingCartProtocol? { get set }
    var navigationDelegate: RootNavigationProtocol? { get set }
}

protocol PresenterToInteractorDetailProtocol: AnyObject {

    var presenter: InteractorToPresenterDetailProtocol? { get set }

    func addToCart(_ item: CartItem)
}

protocol InteractorToPresenterDetailProtocol: AnyObject {

    func addedToCart(_ item: CartItem)
}
