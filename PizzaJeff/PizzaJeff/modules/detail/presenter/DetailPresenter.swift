//
//  DetailPresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class DetailPresenter: ViewToPresenterDetailProtocol, InteractorToPresenterDetailProtocol {

    var view: PresenterToViewDetailProtocol?
    var interactor: PresenterToInteractorDetailProtocol?
    var router: PresenterToRouterDetailProtocol?

    func addToCart(item: MenuItem, price: Price) {
        interactor?.addToCart(
            PizzaCartItem(
                id: item.id,
                price: price.price,
                size: price.size,
                productInfo: ProductInfo(name: item.name, content: item.content, imageUrl: item.imageUrl),
                amount: 1
            ))
    }

    func addedToCart(_ item: CartItem) {
        router?.shoppingCartDelegate?.updateShoppingCart()
    }
}
