//
//  DetailInteractor.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class DetailInteractor: PresenterToInteractorDetailProtocol {

    var presenter: InteractorToPresenterDetailProtocol?

    func addToCart(_ item: CartItem) {
        Session.current?.cart?.add(item)
        presenter?.addedToCart(item)
    }
}
