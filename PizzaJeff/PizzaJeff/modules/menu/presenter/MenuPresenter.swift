//
//  MenuPresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class MenuPresenter: ViewToPresenterMenuProtocol, InteractorToPresenterMenuProtocol {

    var view: PresenterToViewMenuProtocol?
    var interactor: PresenterToInteractorMenuProtocol?
    var router: PresenterToRouterMenuProtocol?

    func retrieveMenu() {
        view?.setLoaderVisibility(visible: true)
        interactor?.retrieveMenu()
    }

    func showDetail(item: MenuItem) {
        router?.goToDetail(item: item)
    }

    func onMenuSuccess(menu: [MenuItemResponse]) {
        view?.setLoaderVisibility(visible: false)
        view?.onMenuSuccess(menu: menu.map({ menuItemResponse in
            MenuItem(response: menuItemResponse)
        }))
    }

    func onMenuError(_ error: RequestError) {
        view?.setLoaderVisibility(visible: false)
        view?.onMenuError(message: "") //TODO description message from error
    }
}
