//
//  MenuRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class MenuRouter: PresenterToRouterMenuProtocol {

    static func createMenuModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> MenuViewController {

        let view = MenuViewController.loadFromNib()
        let presenter = MenuPresenter()
        let interactor: PresenterToInteractorMenuProtocol
        let router = MenuRouter()

        if AppDelegate.mockNetworkRequests {
            interactor = MenuInteractorMock()
        } else {
            interactor = MenuInteractor()
        }

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        router.navigationDelegate = navigationDelegate
        router.shoppingCartDelegate = shoppingCartDelegate

        return view
    }

    var navigationDelegate: RootNavigationProtocol?
    var shoppingCartDelegate: ToolbarShoppingCartProtocol?

    func goToDetail(item: MenuItem) {
        let vc = DetailRouter.createDetailModule(navigationDelegate: navigationDelegate, shoppingCartDelegate: shoppingCartDelegate)
        vc.menuItem = item
        navigationDelegate?.navigateTo(viewController: vc)
    }
}
