//
//  MenuProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

protocol ViewToPresenterMenuProtocol: AnyObject {

    var view: PresenterToViewMenuProtocol? { get set }
    var interactor: PresenterToInteractorMenuProtocol? { get set }
    var router: PresenterToRouterMenuProtocol? { get set }

    func retrieveMenu()
    func showDetail(item: MenuItem)
}

protocol PresenterToViewMenuProtocol: AnyObject {

    var presenter: ViewToPresenterMenuProtocol? { get set }

    func setLoaderVisibility(visible: Bool)
    func onMenuSuccess(menu: [MenuItem])
    func onMenuError(message: String)
}

protocol PresenterToRouterMenuProtocol: AnyObject {

    static func createMenuModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> MenuViewController

    var navigationDelegate: RootNavigationProtocol? { get set }
    var shoppingCartDelegate: ToolbarShoppingCartProtocol? { get set }

    func goToDetail(item: MenuItem)
}

protocol PresenterToInteractorMenuProtocol: AnyObject {

    var presenter: InteractorToPresenterMenuProtocol? { get set }

    func retrieveMenu()
}

protocol InteractorToPresenterMenuProtocol: AnyObject {

    func onMenuSuccess(menu: [MenuItemResponse])
    func onMenuError(_ error: RequestError)
}
