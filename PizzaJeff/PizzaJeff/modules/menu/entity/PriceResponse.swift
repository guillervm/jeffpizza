//
//  PriceResponse.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

struct PriceResponse: Codable {

    let size: String
    let price: Double

    enum CodingKeys: String, CodingKey {
        case size = "size"
        case price = "price"
    }
}
