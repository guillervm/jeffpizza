//
//  MenuItemResponse.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

struct MenuItemResponse: Codable {

    let id: UInt
    let name: String
    let content: String
    let imageUrl: String
    let prices: [PriceResponse]

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case content = "content"
        case imageUrl = "imageUrl"
        case prices = "prices"
    }
}
