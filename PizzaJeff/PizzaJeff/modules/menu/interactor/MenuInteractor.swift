//
//  MenuInteractor.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class MenuInteractor: PresenterToInteractorMenuProtocol {

    var presenter: InteractorToPresenterMenuProtocol?

    func retrieveMenu() {
        guard let url = URL(string: "https://gist.githubusercontent.com/eliseo-juan/c9c124b0899ae9adc254146783c0b764/raw") else {
            presenter?.onMenuError(.badRequest)
            return
        }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    debugPrint("Error: \(error)")
                    self.presenter?.onMenuError(.genericError)
                }
                return
            }
            guard let data = data, let menu = try? JSONDecoder().decode([MenuItemResponse].self, from: data) else {
                DispatchQueue.main.async {
                    self.presenter?.onMenuError(.invalidData)
                }
                return
            }
            DispatchQueue.main.async {
                self.presenter?.onMenuSuccess(menu: menu)
            }
        }
        task.resume()
    }
}
