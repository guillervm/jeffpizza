//
//  MenuInteractorMock.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class MenuInteractorMock: PresenterToInteractorMenuProtocol {

    var presenter: InteractorToPresenterMenuProtocol?

    func retrieveMenu() {
        presenter?.onMenuSuccess(menu: mockMenuResponse())
    }

    private func mockMenuResponse() -> [MenuItemResponse] {
        return [
            MenuItemResponse(id: 1000, name: "Pizza 0", content: "Ingredient 0, ingredient 1 and ingredient 2", imageUrl: "pizza0.png", prices: [
                PriceResponse(size: "S", price: 8),
                PriceResponse(size: "M", price: 10),
                PriceResponse(size: "L", price: 12),
                PriceResponse(size: "XL", price: 15)
            ]),
            MenuItemResponse(id: 1001, name: "Pizza 1", content: "Ingredient 0, ingredient 1 and ingredient 3", imageUrl: "pizza1.png", prices: [
                PriceResponse(size: "S", price: 8),
                PriceResponse(size: "M", price: 10),
                PriceResponse(size: "L", price: 12),
                PriceResponse(size: "XL", price: 15)
            ]),
            MenuItemResponse(id: 1002, name: "Pizza 2", content: "Ingredient 0, ingredient 3 and ingredient 4", imageUrl: "pizza2.png", prices: [
                PriceResponse(size: "S", price: 7),
                PriceResponse(size: "M", price: 9),
                PriceResponse(size: "L", price: 11.5),
                PriceResponse(size: "XL", price: 14)
            ]),
            MenuItemResponse(id: 1003, name: "Pizza 3", content: "Ingredient 1, ingredient 2, ingredient 3 and ingredient 4", imageUrl: "pizza3.png", prices: [
                PriceResponse(size: "L", price: 10),
                PriceResponse(size: "XL", price: 13)
            ]),
            MenuItemResponse(id: 1004, name: "Pizza 4", content: "Ingredient 1, ingredient 2 and ingredient 5", imageUrl: "pizza4.png", prices: [
                PriceResponse(size: "S", price: 9),
                PriceResponse(size: "M", price: 11),
                PriceResponse(size: "L", price: 13),
                PriceResponse(size: "XL", price: 16)
            ])
        ]
    }
}
