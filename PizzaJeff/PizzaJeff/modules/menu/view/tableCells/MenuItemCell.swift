//
//  MenuItemCell.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

final class MenuItemCell: UITableViewCell {

    static let identifier = "MenuItemCell"
    static let nibName = identifier
    static let estimatedHeight: CGFloat = 162
    private static let placeholder: UIImage = #imageLiteral(resourceName: "PizzaPlaceholder")

    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundView = UIView(frame: .zero)
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
    }

    func bind(item: MenuItem) {
        imgMain.loadImage(urlString: item.imageUrl, placeholder: MenuItemCell.placeholder)
        lblName.text = item.name
        lblContent.text = item.content
        lblPrice.text = item.priceDescription
    }
}
