//
//  MenuViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

final class MenuViewController: UIViewController, RootToolbarProtocol, PresenterToViewMenuProtocol, UITableViewDelegate, UITableViewDataSource {

    var presenter: ViewToPresenterMenuProtocol?
    var menu: [MenuItem] = []
    var displayProfile: Bool = true
    var displayShoppingCart: Bool = true
    var toolbarTitle: String = "Pizzas menu"

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorView: UIStackView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicatorView: LoadingIndicator!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: MenuItemCell.nibName, bundle: nil), forCellReuseIdentifier: MenuItemCell.identifier)
        tableView.accessibilityIdentifier = "TablePizzasMenu"
        presenter?.retrieveMenu()
    }

    @IBAction func tryAgainTapped(_ sender: Any) {
        if mainDebouncer.action() {
            setErrorVisibility(visible: false)
            presenter?.retrieveMenu()
        }
    }

    private func setErrorVisibility(visible: Bool) {
        errorView.isHidden = !visible
        tableView.isHidden = !errorView.isHidden
    }

    //MARK: - PresenterToViewMenuProtocol
    func setLoaderVisibility(visible: Bool) {
        guard loadingView.isHidden == visible else {
            return
        }
        loadingView.isHidden = !visible
        if visible {
            loadingIndicatorView.startAnimation()
        } else {
            loadingIndicatorView.stopAnimation()
        }
    }

    func onMenuSuccess(menu: [MenuItem]) {
        self.menu = menu
        tableView.reloadData()
    }

    func onMenuError(message: String) {
        setErrorVisibility(visible: true)
    }

    //MARK: - UITableViewDelegate & UITableViewDataSource
    func cellForMenuItem(_ tableView: UITableView, menuItem: MenuItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuItemCell.identifier) as? MenuItemCell else {
            fatalError("Could not dequeue a cell for \(MenuItemCell.identifier)")
        }
        cell.bind(item: menuItem)
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellForMenuItem(tableView, menuItem: menu[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showDetail(item: menu[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return MenuItemCell.estimatedHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
