//
//  ShoppingCartRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class ShoppingCartRouter: PresenterToRouterShoppingCartProtocol {

    static func createShoppingCartModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> ShoppingCartViewController {

        let view = ShoppingCartViewController.loadFromNib()
        let presenter = ShoppingCartPresenter()
        let interactor: PresenterToInteractorShoppingCartProtocol
        let router = ShoppingCartRouter()

        if AppDelegate.isUiTesting {
            interactor = ShoppingCartInteractorMock()
        } else {
            interactor = ShoppingCartInteractor()
        }

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        router.navigationDelegate = navigationDelegate
        router.shoppingCartDelegate = shoppingCartDelegate

        return view
    }

    var navigationDelegate: RootNavigationProtocol?
    var shoppingCartDelegate: ToolbarShoppingCartProtocol?

    func goToMenu() {
        navigationDelegate?.restart()
    }

    func showMovieModal(delegate: AddMovieProtocol) {
        let vc = AddMovieViewController.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = delegate
        navigationDelegate?.present(vc, animated: true)
    }

    func showSuccessModal() {
        let vc = CheckoutSuccessViewController.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        navigationDelegate?.present(vc, animated: true)
    }
}
