//
//  ShoppingCartProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

protocol ViewToPresenterShoppingCartProtocol: AnyObject {

    var view: PresenterToViewShoppingCartProtocol? { get set }
    var interactor: PresenterToInteractorShoppingCartProtocol? { get set }
    var router: PresenterToRouterShoppingCartProtocol? { get set }

    func checkout()
}

protocol PresenterToViewShoppingCartProtocol: AnyObject {

    var presenter: ViewToPresenterShoppingCartProtocol? { get set }

    func reloadCartItems()
}

protocol PresenterToRouterShoppingCartProtocol: AnyObject {

    static func createShoppingCartModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> ShoppingCartViewController

    var navigationDelegate: RootNavigationProtocol? { get set }
    var shoppingCartDelegate: ToolbarShoppingCartProtocol? { get set }

    func goToMenu()
    func showMovieModal(delegate: AddMovieProtocol)
    func showSuccessModal()
}

protocol PresenterToInteractorShoppingCartProtocol: AnyObject {

    var presenter: InteractorToPresenterShoppingCartProtocol? { get set }

    func checkout()
}

protocol InteractorToPresenterShoppingCartProtocol: AnyObject {

    func checkoutDone()
}
