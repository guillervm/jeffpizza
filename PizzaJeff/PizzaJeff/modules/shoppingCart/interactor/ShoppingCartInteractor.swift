//
//  ShoppingCartInteractor.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class ShoppingCartInteractor: PresenterToInteractorShoppingCartProtocol {

    var presenter: InteractorToPresenterShoppingCartProtocol?

    func checkout() {
        // Throttle to simulate the checkout request
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.presenter?.checkoutDone()
        }
    }
}
