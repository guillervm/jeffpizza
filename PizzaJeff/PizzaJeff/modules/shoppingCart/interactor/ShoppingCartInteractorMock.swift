//
//  ShoppingCartInteractorMock.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class ShoppingCartInteractorMock: PresenterToInteractorShoppingCartProtocol {

    var presenter: InteractorToPresenterShoppingCartProtocol?

    func checkout() {
        presenter?.checkoutDone()
    }
}
