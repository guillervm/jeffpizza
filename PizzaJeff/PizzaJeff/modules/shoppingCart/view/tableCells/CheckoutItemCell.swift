//
//  CheckoutItemCell.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

final class CheckoutItemCell: UITableViewCell {

    static let identifier = "CheckoutItemCell"
    static let nibName = identifier
    static let estimatedHeight: CGFloat = 139

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundView = UIView(frame: .zero)
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
    }

    func bind(item: CartItem) {
        lblName.text = item.productInfo.name
        lblDescription.text = item.description
        lblAmount.text = "\(item.amount) × \(MoneyFormatter.formatAmount(item.price))"
        lblSubtotal.text = MoneyFormatter.formatAmount(item.totalPrice())
    }
}
