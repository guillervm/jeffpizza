//
//  ShoppingCartViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

final class ShoppingCartViewController: UIViewController, RootToolbarProtocol, PresenterToViewShoppingCartProtocol, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblEmptyCart: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var btnCheckout: StandardButton!

    var cartItems: [CartItem] = Session.current?.cart?.items ?? []
    var displayProfile: Bool = false
    var displayShoppingCart: Bool = false
    var toolbarTitle: String = "Checkout"
    var presenter: ViewToPresenterShoppingCartProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.delaysContentTouches = false
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: CheckoutItemCell.nibName, bundle: nil), forCellReuseIdentifier: CheckoutItemCell.identifier)
        tableView.accessibilityIdentifier = "TableCheckoutSummary"
        bind()
    }

    @IBAction func checkoutTapped(_ sender: Any) {
        if mainDebouncer.action() {
            presenter?.checkout()
        }
    }

    func reloadCartItems() {
        cartItems = Session.current?.cart?.items ?? []
        tableView.reloadData()
        bind()
    }

    private func bind() {
        lblTotalAmount.text = MoneyFormatter.formatAmount(Session.current?.cart?.totalAmount ?? 0)
        if cartItems.isEmpty {
            lblEmptyCart.isHidden = false
            tableView.isHidden = true
            btnCheckout.isEnabled = false
            return
        }
    }

    //MARK: - UITableViewDelegate & UITableViewDataSource
    func cellForCheckoutItem(_ tableView: UITableView, cartItem: CartItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutItemCell.identifier) as? CheckoutItemCell else {
            fatalError("Could not dequeue a cell for \(CheckoutItemCell.identifier)")
        }
        cell.bind(item: cartItem)
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellForCheckoutItem(tableView, cartItem: cartItems[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Do nothing, button events will be managed
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return CheckoutItemCell.estimatedHeight
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
