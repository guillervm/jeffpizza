//
//  ShoppingCartPresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

final class ShoppingCartPresenter: ViewToPresenterShoppingCartProtocol, InteractorToPresenterShoppingCartProtocol, AddMovieProtocol {

    var view: PresenterToViewShoppingCartProtocol?
    var interactor: PresenterToInteractorShoppingCartProtocol?
    var router: PresenterToRouterShoppingCartProtocol?

    func checkout() {
        guard Session.current?.user?.segment != .married else {
            router?.showMovieModal(delegate: self)
            return
        }
        doCheckout()
    }

    func checkoutDone() {
        router?.navigationDelegate?.setLoaderVisibility(visible: false)
        router?.showSuccessModal()
        Session.current?.cart?.clear()
        router?.shoppingCartDelegate?.updateShoppingCart()
        DispatchQueue.main.async {
            self.router?.goToMenu()
        }
    }

    private func doCheckout() {
        router?.navigationDelegate?.setLoaderVisibility(visible: true)
        interactor?.checkout()
    }

    //MARK: - AddMovieProtocol
    func movieSelected() {
        // Add a movie to the cart
        Session.current?.cart?.add(CartItem(id: 999, price: 399, productInfo: ProductInfo(name: "Movie", content: "Voucher code for a movie", imageUrl: ""), amount: 1))
        router?.shoppingCartDelegate?.updateShoppingCart()
        view?.reloadCartItems()
    }

    func movieRejected() {
        // Nothing to do
    }

    func dismissed() {
        doCheckout()
    }
}
