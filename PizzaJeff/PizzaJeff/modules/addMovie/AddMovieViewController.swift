//
//  AddMovieViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import UIKit

protocol AddMovieProtocol: AnyObject {

    func movieSelected()
    func movieRejected()
    func dismissed()
}

final class AddMovieViewController: UIViewController {

    weak var delegate: AddMovieProtocol?

    @IBAction func yesTapped(_ sender: Any) {
        if mainDebouncer.action() {
            delegate?.movieSelected()
            dismiss()
        }
    }

    @IBAction func noTapped(_ sender: Any) {
        if mainDebouncer.action() {
            delegate?.movieRejected()
            dismiss()
        }
    }

    func dismiss() {
        dismiss(animated: true) {
            self.delegate?.dismissed()
        }
    }
}
