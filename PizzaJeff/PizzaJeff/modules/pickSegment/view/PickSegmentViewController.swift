//
//  PickSegmentViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

final class PickSegmentViewController: UIViewController, PresenterToViewPickSegmentProtocol {

    var presenter: ViewToPresenterPickSegmentProtocol?

    @IBAction func singleTapped(_ sender: Any) {
        if mainDebouncer.action() {
            startSession(segment: .single)
        }
    }

    @IBAction func marriedTapped(_ sender: Any) {
        if mainDebouncer.action() {
            startSession(segment: .married)
        }
    }

    func dismiss() {
        dismiss(animated: true, completion: nil)
    }

    private func startSession(segment: User.Segment) {
        presenter?.startSession(segment: segment, name: User.randomName())
    }
}
