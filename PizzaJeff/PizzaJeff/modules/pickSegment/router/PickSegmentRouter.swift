//
//  PickSegmentRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class PickSegmentRouter: PresenterToRouterPickSegmentProtocol {

    static func createPickSegmentModule() -> PickSegmentViewController {

        let view = PickSegmentViewController.loadFromNib()
        let presenter = PickSegmentPresenter()
        let interactor = PickSegmentInteractor()
        let router = PickSegmentRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter

        return view
    }
}
