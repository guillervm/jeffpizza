//
//  PickSegmentPresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class PickSegmentPresenter: ViewToPresenterPickSegmentProtocol, InteractorToPresenterPickSegmentProtocol {

    var view: PresenterToViewPickSegmentProtocol?
    var interactor: PresenterToInteractorPickSegmentProtocol?
    var router: PresenterToRouterPickSegmentProtocol?

    func startSession(segment: User.Segment, name: String) {
        interactor?.startSession(segment: segment, name: name)
    }

    func onStartSessionSuccess(user: User) {
        view?.dismiss()
    }
}
