//
//  PickSegmentInteractor.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class PickSegmentInteractor: PresenterToInteractorPickSegmentProtocol {

    var presenter: InteractorToPresenterPickSegmentProtocol?

    func startSession(segment: User.Segment, name: String) {
        let user = User(segment: segment, name: name, promocodes: [])
        Session.startSession(user: user)
        presenter?.onStartSessionSuccess(user: user)
    }
}
