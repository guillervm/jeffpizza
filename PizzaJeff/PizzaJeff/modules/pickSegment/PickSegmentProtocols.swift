//
//  PickSegmentProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

protocol ViewToPresenterPickSegmentProtocol: AnyObject {

    var view: PresenterToViewPickSegmentProtocol? { get set }
    var interactor: PresenterToInteractorPickSegmentProtocol? { get set }
    var router: PresenterToRouterPickSegmentProtocol? { get set }

    func startSession(segment: User.Segment, name: String)
}

protocol PresenterToViewPickSegmentProtocol: AnyObject {

    var presenter: ViewToPresenterPickSegmentProtocol? { get set }

    func dismiss()
}

protocol PresenterToRouterPickSegmentProtocol: AnyObject {

    static func createPickSegmentModule() -> PickSegmentViewController
}

protocol PresenterToInteractorPickSegmentProtocol: AnyObject {

    var presenter: InteractorToPresenterPickSegmentProtocol? { get set }
    func startSession(segment: User.Segment, name: String)
}

protocol InteractorToPresenterPickSegmentProtocol: AnyObject {

    func onStartSessionSuccess(user: User)
}
