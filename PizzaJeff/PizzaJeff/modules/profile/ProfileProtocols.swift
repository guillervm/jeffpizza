//
//  ProfileProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

protocol ViewToPresenterProfileProtocol: AnyObject {

    var view: PresenterToViewProfileProtocol? { get set }
    var interactor: PresenterToInteractorProfileProtocol? { get set }
    var router: PresenterToRouterProfileProtocol? { get set }

    func logout()
}

protocol PresenterToViewProfileProtocol: AnyObject {

    var presenter: ViewToPresenterProfileProtocol? { get set }
}

protocol PresenterToRouterProfileProtocol: AnyObject {

    static func createProfileModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> ProfileViewController

    var navigationDelegate: RootNavigationProtocol? { get set }
    var shoppingCartDelegate: ToolbarShoppingCartProtocol? { get set }

    func goToMenu()
}

protocol PresenterToInteractorProfileProtocol: AnyObject {

    var presenter: InteractorToPresenterProfileProtocol? { get set }

    func logout()
}

protocol InteractorToPresenterProfileProtocol: AnyObject {

    func logoutDone()
}
