//
//  ProfilePresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class ProfilePresenter: ViewToPresenterProfileProtocol, InteractorToPresenterProfileProtocol {

    var view: PresenterToViewProfileProtocol?
    var interactor: PresenterToInteractorProfileProtocol?
    var router: PresenterToRouterProfileProtocol?

    func logout() {
        router?.navigationDelegate?.setLoaderVisibility(visible: true)
        interactor?.logout()
    }

    func logoutDone() {
        router?.navigationDelegate?.setLoaderVisibility(visible: false)
        router?.goToMenu()
    }
}
