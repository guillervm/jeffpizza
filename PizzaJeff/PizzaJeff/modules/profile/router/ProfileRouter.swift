//
//  ProfileRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class ProfileRouter: PresenterToRouterProfileProtocol {

    static func createProfileModule(navigationDelegate: RootNavigationProtocol?, shoppingCartDelegate: ToolbarShoppingCartProtocol?) -> ProfileViewController {

        let view = ProfileViewController.loadFromNib()
        let presenter = ProfilePresenter()
        let interactor: PresenterToInteractorProfileProtocol
        let router = ProfileRouter()

        if AppDelegate.isUiTesting {
            interactor = ProfileInteractorMock()
        } else {
            interactor = ProfileInteractor()
        }

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        router.navigationDelegate = navigationDelegate
        router.shoppingCartDelegate = shoppingCartDelegate

        return view
    }

    var navigationDelegate: RootNavigationProtocol?
    var shoppingCartDelegate: ToolbarShoppingCartProtocol?

    func goToMenu() {
        navigationDelegate?.restart()
    }
}
