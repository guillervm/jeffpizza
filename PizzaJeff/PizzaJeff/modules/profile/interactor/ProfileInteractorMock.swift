//
//  ProfileInteractorMock.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class ProfileInteractorMock: PresenterToInteractorProfileProtocol {

    var presenter: InteractorToPresenterProfileProtocol?

    func logout() {
        Session.current?.endSession()
        self.presenter?.logoutDone()
    }
}
