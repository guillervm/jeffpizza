//
//  ProfileInteractor.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import Foundation

final class ProfileInteractor: PresenterToInteractorProfileProtocol {

    var presenter: InteractorToPresenterProfileProtocol?

    func logout() {
        // Throttle to simulate the logout request
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Session.current?.endSession()
            self.presenter?.logoutDone()
        }
    }
}
