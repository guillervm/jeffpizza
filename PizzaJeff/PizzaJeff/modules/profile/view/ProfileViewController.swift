//
//  ProfileViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import UIKit

final class ProfileViewController: UIViewController, RootToolbarProtocol, PresenterToViewProfileProtocol {

    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var lblSegment: UILabel!

    var presenter: ViewToPresenterProfileProtocol?
    var displayProfile: Bool = false
    var displayShoppingCart: Bool = false
    var toolbarTitle: String = "Profile"

    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = Session.current?.user {
            lblGreeting.text = "Hi \(user.name)!"
            lblSegment.text = "You are \(user.segment.rawValue)!"
        }
    }

    @IBAction func logoutTapped(_ sender: Any) {
        if mainDebouncer.action() {
            presenter?.logout()
        }
    }
}
