//
//  RootRouter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class RootRouter: PresenterToRouterRootProtocol {

    static func setupRootModule(_ view: RootViewController) {
        let presenter = RootPresenter()
        let router = RootRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        router.navigationDelegate = view
        router.shoppingCartDelegate = view
    }

    var navigationDelegate: RootNavigationProtocol?
    var shoppingCartDelegate: ToolbarShoppingCartProtocol?

    func goToPickSegment() {
        let vc = PickSegmentRouter.createPickSegmentModule()
        vc.modalPresentationStyle = .fullScreen
        navigationDelegate?.present(vc, animated: false)
    }

    func goToProfile() {
        let vc = ProfileRouter.createProfileModule(navigationDelegate: navigationDelegate, shoppingCartDelegate: shoppingCartDelegate)
        navigationDelegate?.navigateTo(viewController: vc)
    }

    func goToShoppingCart() {
        let vc = ShoppingCartRouter.createShoppingCartModule(navigationDelegate: navigationDelegate, shoppingCartDelegate: shoppingCartDelegate)
        navigationDelegate?.navigateTo(viewController: vc)
    }
}
