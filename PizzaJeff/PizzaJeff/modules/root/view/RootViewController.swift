//
//  RootViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

protocol RootNavigationProtocol: AnyObject {

    func present(_ viewController: UIViewController, animated: Bool)
    func navigateTo(viewController: UIViewController)
    func restart()
    func setLoaderVisibility(visible: Bool)
}

protocol ToolbarShoppingCartProtocol {

    func updateShoppingCart()
}

protocol RootToolbarProtocol: AnyObject {

    var displayProfile: Bool { get }
    var displayShoppingCart: Bool { get }
    var toolbarTitle: String { get }
}

class RootViewController: UIViewController, ToolbarProtocol, RootNavigationProtocol, ToolbarShoppingCartProtocol, PresenterToViewRootProtocol {

    @IBOutlet weak var toolbar: ToolbarView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicatorView: LoadingIndicator!

    var presenter: ViewToPresenterRootProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        RootRouter.setupRootModule(self)
        toolbar.delegate = self
        reset()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.checkSession()
        updateShoppingCart()
    }

    @objc override func addAsChild(viewController: UIViewController, in containerView: UIView, animated: Bool, completion: (() -> ())? = nil) {
        super.addAsChild(viewController: viewController, in: containerView, animated: animated) {
            completion?()
            self.bindToolbar(viewController: viewController)
        }
    }

    @objc override func remove(viewController: UIViewController, animated: Bool) {
        super.remove(viewController: viewController, animated: animated)
        bindToolbar(viewController: children.last)
    }

    private func evaluateBackButton() {
        changeVisibility(view: toolbar.btnBack, visible: children.count > 1)
    }

    private func changeVisibility(view: UIView, visible: Bool) {
        guard view.isHidden == visible else {
            return
        }
        if visible {
            view.isHidden = false
        }
        UIView.animate(withDuration: Animation.customNavigationDuration / 2) {
            view.alpha = visible ? 1 : 0
        } completion: { _ in
            view.isHidden = !visible
        }
    }

    private func switchTitle(text: String) {
        UIView.animate(withDuration: Animation.customNavigationDuration / 2) {
            self.toolbar.lblTitle.alpha = 0
        } completion: { _ in
            self.toolbar.lblTitle.text = text
            UIView.animate(withDuration: Animation.customNavigationDuration / 2) {
                self.toolbar.lblTitle.alpha = 1
            }
        }
    }

    private func bindToolbar(viewController: UIViewController?) {
        self.evaluateBackButton()
        if let toolbarVc = viewController as? RootToolbarProtocol {
            changeVisibility(view: toolbar.btnProfile, visible: toolbarVc.displayProfile)
            changeVisibility(view: toolbar.containerShoppingCart, visible: toolbarVc.displayShoppingCart)
            switchTitle(text: toolbarVc.toolbarTitle)
        }
    }

    //MARK: - RootNavigationProtocol
    func present(_ viewController: UIViewController, animated: Bool) {
        present(viewController, animated: animated, completion: nil)
    }

    func navigateTo(viewController: UIViewController) {
        addAsChild(viewController: viewController, in: containerView, animated: true)
    }

    func restart() {
        removeAllChildViewControllers()
        DispatchQueue.main.async {
            self.addAsChild(viewController: MenuRouter.createMenuModule(navigationDelegate: self, shoppingCartDelegate: self), in: self.containerView, animated: false)
            self.presenter?.checkSession()
        }
    }

    func setLoaderVisibility(visible: Bool) {
        guard loadingView.isHidden == visible else {
            return
        }
        loadingView.isHidden = !visible
        if visible {
            loadingIndicatorView.startAnimation()
        } else {
            loadingIndicatorView.stopAnimation()
        }
    }

    //MARK: - ToolbarDelegate
    func toolbarBackTapped() {
        if children.count > 1 && mainDebouncer.action() {
            remove(viewController: children[children.count - 1], animated: true)
        }
    }

    func toolbarProfileTapped() {
        if mainDebouncer.action() {
            presenter?.openProfile()
        }
    }

    func toolbarShoppingCartTapped() {
        if mainDebouncer.action() {
            presenter?.openShoppingCart()
        }
    }

    //MARK: - ToolbarShoppingCartProtocol
    func updateShoppingCart() {
        if let itemsCount = Session.current?.cart?.itemsCount() {
            toolbar.viewShoppingCartCounter.isHidden = itemsCount < 1
            toolbar.lblShoppingCartCounter.text = itemsCount > 9 ? "9+" : String(itemsCount)
        } else {
            toolbar.viewShoppingCartCounter.isHidden = true
        }
    }

    //MARK: - PresenterToViewRootProtocol
    func reset() {
        removeAllChildViewControllers()
        updateShoppingCart()
        DispatchQueue.main.async {
            self.addAsChild(viewController: MenuRouter.createMenuModule(navigationDelegate: self, shoppingCartDelegate: self), in: self.containerView, animated: false)
        }
    }
}

