//
//  RootPresenter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class RootPresenter: ViewToPresenterRootProtocol {

    var view: PresenterToViewRootProtocol?
    var router: PresenterToRouterRootProtocol?

    func checkSession() {
        guard let _ = Session.current else {
            router?.goToPickSegment()
            view?.reset()
            return
        }
    }

    func openProfile() {
        router?.goToProfile()
    }

    func openShoppingCart() {
        router?.goToShoppingCart()
    }
}
