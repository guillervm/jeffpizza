//
//  RootProtocols.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

protocol ViewToPresenterRootProtocol: AnyObject {

    var view: PresenterToViewRootProtocol? { get set }
    var router: PresenterToRouterRootProtocol? { get set }

    func checkSession()
    func openProfile()
    func openShoppingCart()
}

protocol PresenterToViewRootProtocol: AnyObject {

    var presenter: ViewToPresenterRootProtocol? { get set }

    func reset()
}

protocol PresenterToRouterRootProtocol: AnyObject {

    var navigationDelegate: RootNavigationProtocol? { get set }
    var shoppingCartDelegate: ToolbarShoppingCartProtocol? { get set }
    
    func goToPickSegment()
    func goToProfile()
    func goToShoppingCart()
}
