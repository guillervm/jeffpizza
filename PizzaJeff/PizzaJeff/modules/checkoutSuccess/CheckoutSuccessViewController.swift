//
//  CheckoutSuccessViewController.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import UIKit

final class CheckoutSuccessViewController: UIViewController {


    @IBAction func dismissTapped(_ sender: Any) {
        if mainDebouncer.action() {
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func okTapped(_ sender: Any) {
        if mainDebouncer.action() {
            dismiss(animated: true, completion: nil)
        }
    }
}
