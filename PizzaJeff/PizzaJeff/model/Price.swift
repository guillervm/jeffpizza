//
//  Price.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

struct Price: Equatable {

    let size: String
    let price: Int // It's a good practice to use Int values as currency amounts, will convert it

    init(response: PriceResponse) {
        size = response.size
        price = Int(response.price * 100)
    }
}
