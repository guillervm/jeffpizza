//
//  CartItem.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

class CartItem: Equatable {

    let id: UInt
    let price: Int
    let productInfo: ProductInfo
    var amount: Int
    var description: String {
        get {
            return ""
        }
    }

    init(id: UInt, price: Int, productInfo: ProductInfo, amount: Int = 0) {
        self.id = id
        self.price = price
        self.productInfo = productInfo
        self.amount = amount >= 0 ? amount : 0
    }

    func add(amount addAmount: Int = 1) {
        amount += addAmount
    }

    func remove(amount removeAmount: Int = 1) {
        guard amount >= removeAmount else {
            amount = 0
            return
        }
        amount -= removeAmount
    }

    func totalPrice() -> Int {
        return price * amount
    }

    func equalsTo(item other: CartItem) -> Bool {
        return self.id == other.id &&
            self.price == other.price
    }

    //MARK: - Equatable
    static func == (lhs: CartItem, rhs: CartItem) -> Bool {
        return lhs.equalsTo(item: rhs)
    }
}
