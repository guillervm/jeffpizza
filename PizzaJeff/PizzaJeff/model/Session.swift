//
//  Session.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class Session {

    static let currency = Currency.euro // Force it to Euro as not provided
    static var current: Session? {
        get {
            return _current
        }
    }
    private static var _current: Session?

    var user: User?
    var cart: Cart?

    private init() {
        //
    }

    @discardableResult static func startSession(user: User) -> Session? {
        Session._current = Session()
        Session._current?.user = user
        Session._current?.cart = Cart()
        return Session.current
    }

    func endSession() {
        Session._current = nil
    }
}
