//
//  User.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class User: Equatable {

    enum Segment: String {
        case single
        case married

        func isPizzaSizeSatisfactory(_ size: String) -> Bool {
            switch self {
            case .single:
                return ["M","L"].contains(size.uppercased())
            case .married:
                return ["L","XL"].contains(size.uppercased())
            }
        }
    }

    private static let names = ["Alison", "Belén", "Carlos", "Daniel", "Ester", "Fernando", "Guillermo", "Helen", "Ira", "Jamie", "Karen", "Laura", "Matthew", "Nikola", "Oksana", "Priyesh", "Quinto", "Rosa", "Samuel", "Teresa", "Ulises", "Vishnu", "William", "Xavier", "Yara", "Zoe"]

    let segment: Segment
    let name: String
    let promocodes: [String]

    init(segment: Segment, name: String = randomName(), promocodes: [String] = []) {
        self.segment = segment
        self.name = name
        self.promocodes = promocodes
    }

    static func randomName() -> String {
        return names[Int(arc4random_uniform(UInt32(names.count)))]
    }

    //MARK: - Equatable
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.segment == rhs.segment &&
            lhs.name == rhs.name &&
            lhs.promocodes == rhs.promocodes
    }
}
