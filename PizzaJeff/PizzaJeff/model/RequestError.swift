//
//  RequestError.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

enum RequestError: String {
    case badRequest = "Bad request"
    case invalidData = "Invalid data"
    case notFound = "Not found"
    case genericError = "Generic error"
}
