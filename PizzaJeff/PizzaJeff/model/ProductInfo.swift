//
//  ProductInfo.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

struct ProductInfo: Equatable {

    let name: String
    let content: String
    let imageUrl: String

    static func == (lhs: ProductInfo, rhs: ProductInfo) -> Bool {
        return lhs.name == rhs.name &&
            lhs.content == rhs.content &&
            lhs.imageUrl == rhs.imageUrl
    }
}
