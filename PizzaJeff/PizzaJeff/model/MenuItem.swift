//
//  MenuItem.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

struct MenuItem {

    let id: UInt
    let name: String
    let content: String
    let imageUrl: String
    let prices: [Price]
    let priceDescription: String

    init(response: MenuItemResponse) {
        id = response.id
        name = response.name
        content = response.content
        imageUrl = response.imageUrl
        prices = response.prices.map({ priceResponse in
            Price(response: priceResponse)
        })
        priceDescription = MenuItem.generatePriceDescription(prices: prices)
    }

    private static func generatePriceDescription(prices: [Price]) -> String {
        guard prices.count > 0 else {
            return "Not available"
        }
        guard prices.count > 1 else {
            return MoneyFormatter.formatAmount(prices[0].price)
        }
        var pricesSorted = prices
        pricesSorted.sort { priceL, priceR in
            priceL.price < priceR.price
        }
        guard let firstPrice = pricesSorted.first, let lastPrice = pricesSorted.last, firstPrice.price != lastPrice.price else {
            return MoneyFormatter.formatAmount(prices[0].price)
        }
        return String(format: "%1$@ - %2$@", MoneyFormatter.formatAmount(firstPrice.price), MoneyFormatter.formatAmount(lastPrice.price))
    }
}
