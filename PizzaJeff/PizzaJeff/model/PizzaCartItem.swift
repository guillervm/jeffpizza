//
//  PizzaCartItem.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class PizzaCartItem: CartItem {

    var size: String
    override var description: String {
        return "Size: \(size)"
    }

    private override init(id: UInt, price: Int, productInfo: ProductInfo, amount: Int = 0) {
        fatalError("Not allowed")
    }

    init(id: UInt, price: Int, size: String, productInfo: ProductInfo, amount: Int = 0) {
        self.size = size
        super.init(id: id, price: price, productInfo: productInfo, amount: amount)
    }

    override func equalsTo(item other: CartItem) -> Bool {
        guard let other = other as? PizzaCartItem else {
            return false
        }
        return super.equalsTo(item: other) &&
            size == other.size
    }
}
