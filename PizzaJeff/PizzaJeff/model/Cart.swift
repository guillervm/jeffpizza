//
//  Cart.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

final class Cart {

    var items: [CartItem]
    var totalAmount: Int {
        get {
            return items.map { item in
                return item.amount * item.price
            }.reduce(0, +)
        }
    }

    init(items: [CartItem] = []) {
        self.items = items
    }

    func add<T: CartItem>(_ addItem: T) {
        if let item = items.first(where: { item in
            addItem == item
        }) {
            item.add(amount: addItem.amount)
            return
        }
        items.append(addItem)
    }

    func remove<T: CartItem>(_ removeItem: T) {
        guard let item = items.first(where: { item in
            removeItem == item
        }) else {
            return
        }
        item.remove(amount: removeItem.amount)
        if item.amount == 0, let index = items.firstIndex(of: item) {
            items.remove(at: index)
        }
    }

    func clear() {
        items.removeAll()
    }

    func itemsCount() -> Int {
        return items.map { item in
            item.amount
        }.reduce(0, +)
    }
}
