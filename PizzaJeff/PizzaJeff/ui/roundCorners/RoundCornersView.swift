//
//  RoundCornersView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

final class RoundCornersView: UIView, RoundCornersViewProtocol {

    override var bounds: CGRect {
        didSet {
            roundCorners()
        }
    }

    override init(frame: CGRect){
        super.init(frame: frame)
        setUpView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }

    func setUpView() {
        layer.masksToBounds = true
        roundCorners()
    }
}
