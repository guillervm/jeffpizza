//
//  RoundCornersProtocol.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

protocol RoundCornersViewProtocol {

    func roundCorners(radius: CGFloat)
}

//MARK:- Default implementation for UIView
extension RoundCornersViewProtocol where Self: UIView {

    func roundCorners(radius: CGFloat = UIView.defaultCornerRadius) {
        self.cornerRadius = radius
    }
}
