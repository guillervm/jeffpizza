//
//  LoadingIndicator.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import UIKit

final class LoadingIndicator: TintedImageView {

    private static let animationStepDuration = 0.5
    private static let scale: CGFloat = 0.6

    override var frame: CGRect {
        didSet {
            calculateTransforms()
        }
    }

    private var transforms: [CGAffineTransform] = []
    private var animatingLoad = false
    private var currentTransform = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    override init(image: UIImage?) {
        super.init(image: image)
        commonInit(image: image)
    }

    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        commonInit(image: image)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit(image: UIImage? = nil) {
        setImage(image ?? #imageLiteral(resourceName: "Pizza"))
        calculateTransforms()
    }

    func startAnimation() {
        transform = .identity
        animatingLoad = true
        animate()
    }

    func stopAnimation() {
        animatingLoad = false
        currentTransform = 0
    }

    private func animate() {
        guard animatingLoad, transforms.count > 0 else {
            return
        }
        UIView.animate(withDuration: LoadingIndicator.animationStepDuration) {
            self.transform = self.transforms[self.currentTransform]
        } completion: { _ in
            self.currentTransform = (self.currentTransform + 1) % self.transforms.count
            self.animate()
        }
    }

    private func calculateTransforms() {
        let translationX = frame.width / 4
        transforms = [
            CGAffineTransform(translationX: -translationX, y: 0).scaledBy(x: LoadingIndicator.scale, y: LoadingIndicator.scale),
            .identity,
            CGAffineTransform(translationX: translationX, y: 0).scaledBy(x: LoadingIndicator.scale, y: LoadingIndicator.scale),
            .identity
        ]
    }
}
