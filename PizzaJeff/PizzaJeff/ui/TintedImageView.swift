//
//  TintedImageView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

class TintedImageView: UIImageView {

    override func prepareForInterfaceBuilder() {
        self.configure()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.configure()
    }

    override var tintColor: UIColor! {
        didSet {
            self.configure()
        }
    }

    func setImage(_ image: UIImage?) {
        self.image = image?.withRenderingMode(.alwaysTemplate)
    }

    private func configure() {
        tintAdjustmentMode = .normal
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
    }
}
