//
//  ToolbarView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

protocol ToolbarProtocol: AnyObject {

    func toolbarBackTapped()
    func toolbarProfileTapped()
    func toolbarShoppingCartTapped()
}

final class ToolbarView: ShadowView {

    private static let nibName = "ToolbarView"

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.text = ""
        }
    }
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var containerShoppingCart: UIView!
    @IBOutlet weak var btnShoppingCart: UIButton!
    @IBOutlet weak var viewShoppingCartCounter: RoundView!
    @IBOutlet weak var lblShoppingCartCounter: UILabel!

    weak var delegate: ToolbarProtocol?

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed(ToolbarView.nibName, owner: self, options: nil)
        contentView.fillContainerView(self)
    }

    @IBAction func backTapped(_ sender: Any) {
        delegate?.toolbarBackTapped()
    }

    @IBAction func profileTapped(_ sender: Any) {
        delegate?.toolbarProfileTapped()
    }

    @IBAction func shoppingCartTapped(_ sender: Any) {
        delegate?.toolbarShoppingCartTapped()
    }
}
