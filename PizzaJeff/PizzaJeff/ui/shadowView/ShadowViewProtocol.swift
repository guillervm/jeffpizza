//
//  ShadowViewProtocol.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

protocol ShadowViewProtocol {
    func applyShadow(color: UIColor, opacity: Float, offset: CGSize, radius: CGFloat)
}

//MARK:- Default implementation for UIView
extension ShadowViewProtocol where Self: UIView {

    func applyShadow(color: UIColor = .black, opacity: Float = 0.3, offset: CGSize = CGSize(width: 0, height: 2), radius: CGFloat = 3) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
