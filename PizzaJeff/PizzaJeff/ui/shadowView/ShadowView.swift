//
//  ShadowView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import UIKit

class ShadowView: UIView, ShadowViewProtocol {

    override var bounds: CGRect {
        didSet {
           applyShadow()
        }
    }

    override init(frame: CGRect){
        super.init(frame: frame)
        setUpView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }

    func setUpView() {
        applyShadow()
    }
}
