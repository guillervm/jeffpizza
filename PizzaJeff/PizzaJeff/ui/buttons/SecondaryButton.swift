//
//  SecondaryButton.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 23/06/2021.
//

import UIKit

final class SecondaryButton: RoundedCornersButton {

    override func setUpButton() {
        super.setUpButton()
        buttonBackgroundColorWhenEnabled = .buttonSecondaryBackground
        buttonBackgroundColorWhenSelected = .buttonSecondaryBackgroundHighlighted
        buttonBackgroundColorWhenHighlighted = .buttonSecondaryBackgroundHighlighted
        buttonBackgroundColorWhenDisabled = .buttonSecondaryBackgroundDisabled
        buttonTitleColorWhenEnabled = .buttonTextSecondary
        buttonTitleColorWhenSelected = .buttonTextSecondaryHighlighted
        buttonTitleColorWhenHighlighted = .buttonTextSecondaryHighlighted
        buttonTitleColorWhenDisabled = .buttonTextSecondaryDisabled
        buttonBorderWidth = UIView.defaultBorderWidth
        buttonBorderColor = .buttonSecondaryBorder
    }
}
