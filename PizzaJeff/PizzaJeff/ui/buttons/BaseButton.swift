//
//  BaseButton.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

class BaseButton: UIButton {

    // Class Variables
    var buttonBackgroundColorWhenHighlighted: UIColor?

    var enableTouchRecognizers: Bool = false {
        didSet {
            applySelection()
        }
    }

    var buttonImage: UIImage? {
        didSet {
            applySelection()
        }
    }

    var buttonBackgroundColorWhenEnabled: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonBackgroundColorWhenSelected: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonBackgroundColorWhenDisabled: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonTitleColorWhenSelected: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonTitleColorWhenHighlighted: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonTitleColorWhenEnabled: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonTitleColorWhenDisabled: UIColor? {
        didSet {
            applySelection()
        }
    }

    var buttonTitleFont: UIFont? = UIFont.latoBold() {
        didSet {
            applySelection()
        }
    }

    var buttonCornerRadius: CGFloat? {
        didSet {
            applySelection()
        }
    }

    var buttonBorderWidth: CGFloat? {
        didSet {
            applySelection()
        }
    }

    var buttonBorderColor: UIColor? {
        didSet {
            applySelection()
        }
    }

    //Override variables
    override var isEnabled: Bool {
        didSet {
            applySelection()
        }
    }

    override var isHighlighted: Bool {
        didSet {
            applySelection()
        }
    }

    override var isSelected: Bool {
        didSet {
            applySelection()
        }
    }

    override var buttonType: UIButton.ButtonType {
        get {
            return .custom
        }
    }

    // Init
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpButton()
        applySelection()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpButton()
        applySelection()
    }

    func setUpButton() {
        //
    }

    private func applySelection() {
        setUpBackgroundColor()
        setUpButtonTitleColor()
        setUpCornerRadius()
        setUpButtonBorder()
        setUpButtonImage()
        setUpTouchRecognizers()
    }

    private func setUpButtonBorder() {
        guard let borderWidth = self.buttonBorderWidth, let borderColor = self.buttonBorderColor else { return }
        self.borderColor = borderColor
        self.borderWidth = borderWidth
    }

    private func setUpCornerRadius() {
        guard let buttonCornerRadius = self.buttonCornerRadius else { return }
        self.cornerRadius = buttonCornerRadius
    }

    private func setUpButtonTitleColor() {
        self.titleLabel?.font = buttonTitleFont ?? UIFont.defaultFont()
        self.setTitleColor(buttonTitleColorWhenHighlighted, for: .highlighted)
        self.setTitleColor(buttonTitleColorWhenEnabled, for: .normal)
        self.setTitleColor(buttonTitleColorWhenSelected, for: .selected)
        self.setTitleColor(buttonTitleColorWhenDisabled, for: .disabled)
    }

    private func setUpBackgroundColor() {
        if self.isEnabled {
            if self.isSelected {
                backgroundColor = buttonBackgroundColorWhenSelected
            } else {
                backgroundColor = self.isHighlighted ? buttonBackgroundColorWhenHighlighted : buttonBackgroundColorWhenEnabled
            }
        } else {
            backgroundColor = buttonBackgroundColorWhenDisabled
        }
    }

    private func setUpButtonImage() {
        self.adjustsImageWhenHighlighted = false
        self.setImage(buttonImage, for: .normal)
    }

    private func setUpTouchRecognizers() {
        if enableTouchRecognizers {
            addTouchRecognizer()
        }
    }

    private func addTouchRecognizer() {
        addTarget(self, action: #selector(self.viewTouchUp), for: [.touchUpInside])
        addTarget(self, action: #selector(self.viewTouchDown), for: [ .touchDown])
        addTarget(self, action: #selector(self.viewTouchUp), for: [ .touchUpOutside])
    }

    @objc func viewTouchUp() {
        //
    }

    @objc func viewTouchDown() {
        //
    }
}
