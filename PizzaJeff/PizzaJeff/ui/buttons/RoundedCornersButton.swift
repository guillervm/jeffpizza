//
//  RoundedCornersButton.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

class RoundedCornersButton: BaseButton {

    override func setUpButton() {
        super.setUpButton()
        roundCorners()
    }
}

extension RoundedCornersButton: RoundCornersViewProtocol {}
