//
//  StandardButton.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

final class StandardButton: RoundedCornersButton {

    override func setUpButton() {
        super.setUpButton()
        buttonBackgroundColorWhenEnabled = .buttonStandardBackground
        buttonBackgroundColorWhenSelected = .buttonStandardBackgroundHighlighted
        buttonBackgroundColorWhenHighlighted = .buttonStandardBackgroundHighlighted
        buttonBackgroundColorWhenDisabled = .buttonStandardBackgroundDisabled
        buttonTitleColorWhenEnabled = .buttonTextMain
        buttonTitleColorWhenSelected = .buttonTextHighlighted
        buttonTitleColorWhenHighlighted = .buttonTextHighlighted
        buttonTitleColorWhenDisabled = .buttonTextDisabled
    }
}
