//
//  RoundViewProtocol.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

protocol RoundViewProtocol {
    func makeViewRounded()
}

//MARK:- Default implementation for UIView
extension RoundViewProtocol where Self: UIView {

    func makeViewRounded() {
        self.cornerRadius = min(self.frame.height, self.frame.width) / 2
        self.clipsToBounds = true
    }
}
