//
//  RoundView.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 20/06/2021.
//

import UIKit

class RoundView: UIView, RoundViewProtocol {

    override var bounds: CGRect {
        didSet {
            makeViewRounded()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        makeViewRounded()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        makeViewRounded()
    }
}
