//
//  Debouncer.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import Foundation

let mainDebouncer = Debouncer()

final class Debouncer {

    static let debounceDefault = 500

    private var debounceTimeMillis: Int
    private var lastAction = 0

    init(debounceTimeMillis: Int = Debouncer.debounceDefault) {
        self.debounceTimeMillis = debounceTimeMillis
    }

    func action() -> Bool {
        if AppDelegate.isUiTesting {
            return true
        }
        if Date().timeMillisSince1970() > lastAction + debounceTimeMillis {
            lastAction = Date().timeMillisSince1970()
            return true
        }
        return false
    }
}
