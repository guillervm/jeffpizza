//
//  Constants.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 21/06/2021.
//

import UIKit

struct Animation {

    static let customNavigationDuration = 0.2
}

struct CustomFont {

    static let defaultFontSize: CGFloat = 16.0
    static let latoLight: String = "Lato-Light"
    static let latoRegular: String = "Lato-Regular"
    static let latoBold: String = "Lato-Bold"
}
