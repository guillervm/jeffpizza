//
//  MoneyFormatter.swift
//  PizzaJeff
//
//  Created by Guillermo Velez de Mendizabal on 22/06/2021.
//

import Foundation

enum Currency: String {

    case euro = "EUR"

    var decimals: Int {
        get {
            return 2
        }
    }

    var subunitDivider: Int {
        get {
            return Int(pow(10, Double(decimals)))
        }
    }
}

final class MoneyFormatter {

    private init() {
        //
    }

    static func formatAmount(_ amount: Int, currency: Currency = Session.currency) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyCode = currency.rawValue
        numberFormatter.minimumFractionDigits = currency.decimals
        numberFormatter.maximumFractionDigits = currency.decimals
        let amountToFormat = Float(amount) / Float(currency.subunitDivider)
        return String(format: "%@", numberFormatter.string(from: amountToFormat as NSNumber) ?? String(amountToFormat))
    }
}
